//
//  wordclock.ino
//  WordClock
//
//  Created by Graeme Lyon on 11/08/2016.
//  Copyright © 2016 GRAEMELYON
//
//  Developed with [embedXcode]
//

// Libraries
#include <Arduino.h>
#include <Wire.h>
#include <RTClib.h>
#include <Adafruit_NeoPixel.h>
#include "wordclock.h"


// Constants
#define LED_DATA_PIN PB2
#define LED_STRIP_LEN 1
#define TIMER_COUNT 12499 // 20 Hz = (16MHz / pre=64) / hz=20 - 1
#define AUTO_BRIGHTNESS_MIN 30
#define AUTO_BRIGHTNESS_MAX 128
// 10 bit range - Q10.5
#define FP_SHIFT 5
#define WLPF 4

// Globals
RTC_DS1307 RTC;
// NB: Constructor configures pin mode for data line
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_STRIP_LEN, LED_DATA_PIN, NEO_GRB + NEO_KHZ800);
int ldr_val;
byte autobrightness;
uint8_t cur_sec;


void setup() {
    
    
     // Setup pins
     pinMode(A0, INPUT); // LDR
     
     
     // Configure the timer to read LDR at 20Hz
     // Avoid interrupts as the NeoPixel lib disables them temporarily
     TCCR1A = 0x00;                          // normal mode (not the default because of wiring)
     TCCR1B &= 0xF8;                         // clear prescaler bits
     TCCR1B |= (1 << CS11) | (1 << CS10);    // set presecaler to 64
     TCCR1B |= (1 << WGM12);                 // set 'Clear on Timer Compare' mode
     OCR1A = TIMER_COUNT;                    // set CTC compare value
 
     // Setup RTC
     Serial.begin(57600);
     Wire.begin();
     RTC.begin();
     if (!RTC.isrunning()) {
         Serial.println("RTC is NOT running!");
         // RTC.adjust(DateTime(__DATE__, __TIME__));
     }
     DateTime now = RTC.now();
     cur_sec = now.second();
     
     
     // Read initial LDR value
     ldr_val = analogRead(A0) << FP_SHIFT;
    
    // Initialise LED strip (off)
    strip.begin();
    strip.show();
}

void loop() {
    
    
    /* Read room brightness & set autobrightness */
    // Check if timer flag has been cleared
    if (TIFR1 & (1 << OCF1A)) {
        
        // clear the CTC flag (writing a logic one clears it)
        TIFR1 = (1 << OCF1A);
        
        // Filter new sensor data
        ldr_val += ((analogRead(A0) << FP_SHIFT) - ldr_val) >> WLPF;
        Serial.println(ldr_val >> FP_SHIFT);
        
        // Set auto brightness based on current LDR value
        // This is a linear mapping - alternatively use lookup table for curve response?
        autobrightness = map(ldr_val >> FP_SHIFT, 0, 1024, AUTO_BRIGHTNESS_MIN, AUTO_BRIGHTNESS_MAX);
    }
    
    
    /* Update display if another second has elapsed */
    // Get current time
    DateTime now = RTC.now();
    if (now.second() != cur_sec) {

        // Set which pixels to light
        processHour(now.hour());
        processMinute(now.minute());

        // Show current strip
        strip.show();
        cur_sec = now.second();
    }
    
    
}


void processHour(uint8_t hour) {
    switch (hour) {
        case 0:
        case 12:
            setHourTwelve();
            break;
        case 1:
        case 13:
            SetHourOne();
            break;
        case 2:
        case 14:
            setHourTwo();
            break;
        case 3:
        case 15:
            setHourThree();
            break;
        case 4:
        case 16:
            setHourFour();
            break;
        case 5:
        case 17:
            setHourFive();
            break;
        case 6:
        case 18:
            setHourSix();
            break;
        case 7:
        case 19:
            setHourSeven();
            break;
        case 8:
        case 20:
            setHourEight();
            break;
        case 9:
        case 21:
            setHourNine();
            break;
        case 10:
        case 22:
            setHourTen();
            break;
        case 11:
        case 23:
            setHourEleven();
            break;
        default:
            Serial.print("Hour mismatch h = ");
            Serial.print(hour, DEC);
            Serial.println();
            break;
    }
}


void processMinute(uint8_t minute) {

    // Set minute word(s)
    if ((minute >=5 && minute <10) || (minute >=55)) {
        // five
        setMinFive();
    } else if ((minute >=10 && minute <15) || (minute >=50 && minute <55)) {
        // ten
        setMinTen();
    } else if ((minute >=15 && minute <20) || (minute >=45 && minute <50)) {
        // quarter
        setMinQuarter();
    } else if ((minute >=20 && minute <25) || (minute >=40 && minute <45)) {
        // twenty
        setMinTwenty();
    } else if ((minute >=25 && minute <10) || (minute >=35 && minute <40)) {
        // twenty five
        setMinTwenty();
        setMinFive();
    } else if (minute >=30 && minute <35) {
        // half
        setMinHalf();
    }

    // Set past/to the hour
    if (minute < 35) {
        setWordPast();
    } else {
        setWordTo();
    }

}


void setPixel(uint16_t n, uint16_t red, uint16_t green, uint16_t blue) {
     // Get color ratios 0-1
     // Set brightness using autobrightness
    red *= autobrightness;
    green *= autobrightness;
    blue *= autobrightness;
    strip.setPixelColor(n, red, green, blue);
}


// EOF
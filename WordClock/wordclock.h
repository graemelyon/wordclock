//
//  wordclock.h
//  WordClock
//
//  Created by Graeme Lyon on 11/08/2016.
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#ifndef wordclock_h
#define wordclock_h

void processHour(uint8_t hour);
void processMinute(uint8_t minute);
void setPixel(uint16_t n, uint8_t r, uint8_t g, uint8_t b);

// Functions to light up individual words
void setHourTwelve(void);
void SetHourOne(void);
void setHourTwo(void);
void setHourThree(void);
void setHourFour(void);
void setHourFive(void);
void setHourSix(void);
void setHourSeven(void);
void setHourEight(void);
void setHourNine(void);
void setHourTen(void);
void setHourEleven(void);
void setMinFive(void);
void setMinTen(void);
void setMinQuarter(void);
void setMinTwenty(void);
void setMinHalf(void);
void setWordPast();
void setWordTo();

#endif /* wordclock_h */
